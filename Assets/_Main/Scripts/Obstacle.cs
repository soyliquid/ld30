﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	public GameObject DestroyEffectPrefab;
	private Vector3 rotateAxis;

	void Start() {
		rotateAxis = Random.onUnitSphere;
	}

	void Update() {
		transform.Rotate(rotateAxis, 1f);
	}

	void OnCollisionEnter(Collision c) {
		// On shoot down.
		if(c.gameObject.layer == LayerMask.NameToLayer("Projectile")) {
			Instantiate(DestroyEffectPrefab, transform.position, Quaternion.identity);
			GameManager.Instance.Score++;
			Destroy(gameObject);
		}
		// On land to homeworld.
		if(c.gameObject.layer == LayerMask.NameToLayer("HomePlanet")) {
			GameManager.Instance.Gameover();
		}

	}

}
