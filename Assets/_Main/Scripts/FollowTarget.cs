﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {

	public Transform Target;
	public float Smooth = .2f;

	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp(transform.position, Target.position, Smooth);
		transform.rotation = Quaternion.Lerp(transform.rotation, Target.rotation, Smooth);
	}

	public void SetTarget(Transform newTarget) {
		Target = newTarget;
	}
}
