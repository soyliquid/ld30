﻿using UnityEngine;
using System.Collections;

public class LookTarget : MonoBehaviour {

	public Transform Target;
	public bool IsReverse;

	// Update is called once per frame
	void Update () {
		transform.LookAt(Target.position);		
		if(IsReverse) {
			transform.Rotate(0f, 180, 0f);
		}
	}
}
