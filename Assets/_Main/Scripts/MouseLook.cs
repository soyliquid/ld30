﻿using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour {

	public Transform HorizontalTarget;
	public Transform VerticalTarget;
	private float speed = 3f;
	private float verticalAngleLimit = 60f;
	private float currentVerticalAngle;

	// Update is called once per frame
	void Update () {		
		// Can control in Main state.
		if(GameManager.Instance.GetCurrentGameState() != GameState.Main) {
			return;
		}

		// Get mouse movement for look control..
		float xSpeed = Input.GetAxis("Mouse X") * speed;
		float ySpeed = Input.GetAxis("Mouse Y") * speed;

		// Simply rotate horizontal angle.
		HorizontalTarget.Rotate(transform.up, xSpeed, Space.World);

		// Restrict vertical angle.
		currentVerticalAngle += -ySpeed;
		if(currentVerticalAngle < verticalAngleLimit && 
		   currentVerticalAngle > -verticalAngleLimit) {		
			VerticalTarget.Rotate(transform.right, -ySpeed, Space.World);			
		} else {
			currentVerticalAngle = Mathf.Clamp(
				currentVerticalAngle,
				-verticalAngleLimit, 
				verticalAngleLimit);
		}
	}
}
