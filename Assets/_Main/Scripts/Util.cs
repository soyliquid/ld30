﻿using UnityEngine;
using System.Collections;

public static class Util {

	public static IEnumerator ShakeObject(Transform t, float power, float duration) {
		float startTime = Time.time;
		while(startTime + duration > Time.time) {
			float progress = ((startTime + duration) - Time.time + 0.1f) / duration;
			t.localPosition = Random.insideUnitSphere * power * progress;
			yield return new WaitForSeconds(0.01f);
		}
		t.localPosition = Vector3.zero;
	}

}
