﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class makes binds with AudioClip and real data.
/// </summary>
public class SoundManager : MonoBehaviour {
	
	// Singleton.
	public static SoundManager Instance {
		get { return _instance; }
	}
	private static SoundManager _instance;

	public AudioClip AcceptSE;
	public AudioClip CancelSE;
	public AudioClip ShotSE;
	public AudioClip DestroySE;
	public AudioClip BeepSE;
	public AudioClip CrashSE;

	void Start () {
		_instance = this;
	}
}
