﻿using UnityEngine;
using System.Collections;

public enum GameState {
	Title,
	Main,
	Result
}

public class GameManager : MonoBehaviour {

	// Singleton.
	public static GameManager Instance {
		get { return _instance; }
	}
	private static GameManager _instance;
	private GameState currentGameState;
	
	[Header("Prefab")]
	public GameObject ShotPrefab;
	public GameObject ObstaclePrefab;

	[Header("UI")]
	public GameObject TitleUI;
	public GameObject GameUI;
	public GameObject ResultUI;
	public TextMesh ScoreText;
	public TextMesh ResultText;
	public Renderer Transition;

	[Header("GameElements")]
	public GameObject HomePlanet;
	public GameObject HomePlanetDestroyEffect;
	public GameObject ProjectileGroup;
	public GameObject EnemyGroup;
	public GameObject CameraObject;
	public GameObject SubCameraObject;
	public Transform GameOverCameraPosition;

	public int Score;

	private float obstacleSpawnDelay = 3f;
	private float minObstacleSpawnDelay = .5f;
	private float obstacleSpawnDistance = 100f;
	private float obstacleSpeed = 3f;
	private float nextObstacleSpawnTime;

	public GameState GetCurrentGameState() {
		return currentGameState;
	}

	void Start () {
		_instance = this;
	}
	
	void Update () {
		Screen.showCursor = false;
		Screen.lockCursor = true;

		switch(currentGameState) {
		case GameState.Title:
			InputInTitle();
			break;
		case GameState.Main:
			InputInMain();
			GameProcess();
			break;
		case GameState.Result:
			InputInResult();
			break;
		}
	}
	
	void InputInTitle()  {
		if(Input.GetKeyDown(KeyCode.Space)) {
			Camera.main.audio.PlayOneShot(SoundManager.Instance.AcceptSE);
			TitleUI.SetActive(false);
			GameUI.SetActive(true);
			currentGameState = GameState.Main;
		}
	}

	void InputInMain()  {
		
	}

	void GameProcess() {

		ScoreText.text = "Score: " + Score;
		ResultText.text = "Score: " + Score;

		// Enemy spawn.
		if(nextObstacleSpawnTime < Time.time) {
			// Set next spawn time shorter than now.
			obstacleSpawnDelay *= 0.95f;
			if(obstacleSpawnDelay < minObstacleSpawnDelay) {
				obstacleSpawnDelay = minObstacleSpawnDelay;
			}
			nextObstacleSpawnTime = Time.time + obstacleSpawnDelay;

			// Spawn new enemy at random position.
			GameObject o = (GameObject)Instantiate(
				GameManager.Instance.ObstaclePrefab, 
				HomePlanet.transform.position + Random.onUnitSphere * obstacleSpawnDistance, 
				Quaternion.identity);
			// Head to homeplanet.
			o.rigidbody.velocity = (HomePlanet.transform.position - o.transform.position).normalized * obstacleSpeed;
			// Set Parent for Tidy Hherarchy view and future group-target process.
			o.transform.parent = EnemyGroup.transform;
			// Randomize color.
			o.renderer.material.SetColor("_TintColor", new Color(
				Random.Range(2f, 10f) / 10f,
				Random.Range(2f, 10f) / 10f,
				Random.Range(2f, 10f) / 10f));
			o.transform.localScale *= Random.Range(10f, 20f) / 10f;
		}

	}

	public void Gameover() {
		// Prepare for gameover.
		CameraObject.SendMessage("SetTarget", GameOverCameraPosition);
		SubCameraObject.SetActive(false);
		
		Camera.main.audio.PlayOneShot(SoundManager.Instance.BeepSE);
		GameUI.SetActive(false);
		ResultUI.SetActive(true);
		currentGameState = GameState.Result;

		// Stop all enemy's move and sound.
		foreach(Transform t in EnemyGroup.GetComponentInChildren<Transform>()) {
			t.rigidbody.velocity = Vector3.zero;
			t.audio.Stop();
		}
		// Show gameover effect .
		StartCoroutine(GameoverEffect());
	}

	IEnumerator GameoverEffect() {
		// Fade to white, and banish, then blow homeworld.
		yield return new WaitForSeconds(1f);
		int fadeWeight = 20;
		for(int idx = 0; idx < fadeWeight; idx++) {
			Transition.material.SetColor("_TintColor", new Color(1f, 1f, 1f, idx / (float)fadeWeight));
			yield return new WaitForSeconds(0.01f);
		}
		Transition.material.SetColor("_TintColor", new Color(1f, 1f, 1f, 0f));
		StartCoroutine(Util.ShakeObject(Camera.main.transform, 1f, 1f));
		Instantiate(HomePlanetDestroyEffect, HomePlanet.transform.position, Quaternion.identity);
		Destroy(HomePlanet);
	}

	void InputInResult()  {
		if(Input.GetKeyDown(KeyCode.R)) {
			Application.LoadLevel(Application.loadedLevelName);
		}
	}
}
