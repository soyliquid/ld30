﻿using UnityEngine;
using System.Collections;

public class MotionObject : MonoBehaviour {

	public Quaternion RotateSpeed;

	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(RotateSpeed.x, RotateSpeed.y, RotateSpeed.z), RotateSpeed.w);
	}
}
