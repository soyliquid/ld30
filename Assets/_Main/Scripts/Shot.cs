﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {

	public GameObject DestroyEffectPrefab;

	void Start() {
		// Auto destroy after 20 seconds.
		Destroy(gameObject, 20f);
	}

	void OnCollisionEnter(Collision c) {
		Instantiate(DestroyEffectPrefab, c.contacts[0].point, Quaternion.identity);
		Destroy(gameObject);
	}
}
