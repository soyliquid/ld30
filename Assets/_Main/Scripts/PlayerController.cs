﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public Transform PlanetPivot;
	public Transform Muzzle;
	private float speed = 12f;
//	private float jumpSpeed = 5f;
//	private float gravity = 0.2f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		// Can control in Main state.
		if(GameManager.Instance.GetCurrentGameState() != GameState.Main) {
			return;
		}

		// Move world surface
		Vector3 inputVector = 
			transform.right * Input.GetAxis("Vertical") + 
			transform.forward * -Input.GetAxis("Horizontal");

		// Normalize for diagonal move.
		if(inputVector.magnitude > 1f) {
			inputVector = inputVector.normalized;
		}

		// Dash
//		float actualSpeed = Speed;
//		if(Input.GetKey(KeyCode.LeftShift)) {
//			actualSpeed *= 2f;
//		}

		// Update position on world surface.
		transform.RotateAround(
			PlanetPivot.position, 
			inputVector, 
			inputVector.magnitude * speed / GetGravityDistance());// * Time.deltaTime * 30f);

		// Stand Straightly.
		//transform.rotation.SetLookRotation(PlanetPivot.position, GetGravityDirection());
//		transform.rotation = Quaternion.LookRotation(Vector3.forward, GetGravityDirection());
		//transform.Rotate(90f, 0f, 0f);

		// Jump *unstable
//		if(Input.GetKeyDown(KeyCode.Space)) {
//			rigidbody.velocity -= GetGravityDirection() * JumpSpeed;
//		}		

		// Gravity to Pivot		
//		rigidbody.velocity += GetGravityDirection() * Gravity;

		// Fire
		if(Input.GetKeyDown(KeyCode.Mouse0)) {
			GameObject o = (GameObject)Instantiate(GameManager.Instance.ShotPrefab, Muzzle.position, Muzzle.rotation);
			o.rigidbody.velocity = Muzzle.transform.forward * 30f;
			o.transform.parent = GameManager.Instance.ProjectileGroup.transform;
			Camera.main.audio.PlayOneShot(SoundManager.Instance.ShotSE);
		}

	}
	
	Vector3 GetGravityDirection() {
		return (PlanetPivot.position - transform.position).normalized;
	}
	float GetGravityDistance() {
		return (PlanetPivot.position - transform.position).magnitude;
	}

}
